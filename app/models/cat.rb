# == Schema Information
#
# Table name: cats
#
#  id          :integer          not null, primary key
#  birth_date  :date
#  color       :string           not null
#  name        :string           not null
#  sex         :string(1)        not null
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer          not null
#

class Cat < ActiveRecord::Base

  CAT_COLORS = [
    "Black",
    "Orange",
    "Calico",
    "Hazel",
    "White",
    "Gray",
    "Multicolored",
    "Hairless"
  ]

  validates :color, presence: true, inclusion: { in: CAT_COLORS }
  validates :sex, presence: true, inclusion: { in: ["M", "F"] }
  validates :name, presence: true

  has_many(
    :cat_rental_requests,
    class_name: 'CatRentalRequest',
    foreign_key: :cat_id,
    primary_key: :id,
    dependent: :destroy
  )

  belongs_to(
    :owner,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id
  )

  def age
    return false if birth_date.nil?
    ((Date.today - birth_date) / 365).to_i
  end



end
