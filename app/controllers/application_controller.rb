class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :require_current_user!,
                :redirect_logged_in_users!, :require_owner!

  def login!(user)
    return nil if user.nil?
    @current_user = user
    session[:session_token] = user.session_token
  end

  def current_user
    return nil if session[:session_token].nil?
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def require_current_user!
    redirect_to new_session_url if current_user.nil?
  end

  def redirect_logged_in_users!
    redirect_to cats_url unless current_user.nil?
  end

  def require_owner!
    cat = Cat.find(params[:id])
    unless cat.user_id == current_user.id
      flash[:error] = "You do not have permission to do that."
      redirect_to cats_url
    end
  end

end
