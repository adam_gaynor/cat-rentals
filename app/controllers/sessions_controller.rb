class SessionsController < ApplicationController

  before_action :redirect_logged_in_users!, except: :destroy

  def new
    #@user = User.new

    render :new
  end

  def create
    user = User.find_by_credentials(
      params[:user_name],
      params[:password]
    )

    if user.nil?
      flash.now[:error] = "Invalid credentials"
      render :new
    else
      login!(user)
      flash[:notice] = "Welcome!"
      redirect_to cats_url
    end

  end

  def destroy
    current_user.reset_session_token!
    session[:session_token] = nil
    redirect_to new_session_url
  end

end
