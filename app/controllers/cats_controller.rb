class CatsController < ApplicationController

  before_action :require_current_user!
  before_action :require_owner!, except: [:index, :new, :create, :show]
  def index
    @cats = Cat.all
    render :index
  end

  def new
    #dummy object
    @cat = Cat.new
    render :new
  end

  def create
    @cat = Cat.new(cat_params)
    @cat.user_id = current_user.id
    if @cat.save
      redirect_to cat_url(@cat)
    else
      render :new
    end
  end

  def show
    @cat = Cat.find(params[:id])
    render :show
  end

  def edit
    @cat = Cat.find(params[:id])
    render :edit
  end

  def update
    @cat = Cat.find(params[:id])
    if @cat.update(cat_params)
      redirect_to cat_url(@cat)
    else
      render :edit
    end
  end

  def destroy
    @cat = Cat.find(params[:id])
    @cat.destroy!

    redirect_to cats_url
  end


  private
  def cat_params
    params.require(:cat).permit(:name, :birth_date, :color, :sex, :description)
  end

end
