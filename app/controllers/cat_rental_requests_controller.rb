class CatRentalRequestsController < ApplicationController

  before_action :require_current_user!
  before_action :require_owner!, except: [:new, :create, :show]

  def approve
    current_cat_rental_request.approve!
    redirect_to cat_url(current_cat)
  end

  def deny
    current_cat_rental_request.deny!
    redirect_to cat_url(current_cat)
  end

  def new
    @request = CatRentalRequest.new
    render :new
  end

  def create
    @request = CatRentalRequest.new(cat_rental_requests_params)
    @request.user_id = current_user.id
    if @request.save
      redirect_to cat_url(@request.cat)
    else
      flash.now[:errors] = @request.errors.full_messages
      render :new
    end
  end

  def show
    @request = CatRentalRequest.find(params[:id])
  end


  private
  def current_cat_rental_request
    @request ||=
      CatRentalRequest.includes(:cat).find(params[:id])
  end

  def current_cat
    current_cat_rental_request.cat
  end

  def cat_rental_requests_params
    params.require(:request).permit(:cat_id, :start_date, :end_date, :status)
  end

  def require_owner!
    request = CatRentalRequest.find(params[:id])
    unless request.cat.user_id == current_user.id
      flash[:error] = "You do not have permission to do that."
      redirect_to cats_url
    end
  end
end
