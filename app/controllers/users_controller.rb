class UsersController < ApplicationController

  before_action :redirect_logged_in_users!


  def show

  end

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login!(@user)
      redirect_to cats_url
      #redirect_to user_url(@user)
    else
      render :new
    end
  end

  def edit

  end

  def update

  end


  def destroy

  end

  private
  def user_params
    params.require(:user).permit(:user_name, :password)
  end
end
